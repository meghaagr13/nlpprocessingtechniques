from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfVectorizer
import math
import numpy
from sklearn.feature_extraction.text import TfidfTransformer
from gensim.models import Word2Vec
from gensim.scripts.glove2word2vec import glove2word2vec
from gensim.models import KeyedVectors

def bagofword(corpus) : 
    vec = CountVectorizer()
    out = vec.fit_transform(corpus).toarray()
    vocab = vec.vocabulary_
    print(len(vocab))
#    print(sorted(((v, k) for k,v in vec.vocabulary_.items())))
#    print out[0] 
    return out
#def normalizetf():
        
def term_frequency(corpus):
    a= bagofword(corpus)
    a=a.astype(float)
    normalized = a.T/a.sum(axis=1)
    return normalized.T

def idf(corpus):
    idf_values = {}
    #print corpus
    all_tokens_set = set([word for doc in corpus for word in doc.split()])
    #print all_tokens_set
    for tkn in all_tokens_set:
        contains_token = map(lambda doc: tkn in doc, corpus)
        idf_values[tkn] = math.log(len(corpus)/(sum(contains_token)*1.0))
    return idf_values

def tfidf_auto(corpus):
    out = bagofword(corpus)
    tfidf = TfidfTransformer(norm="l2",sublinear_tf=True,smooth_idf=True)
    tfidf.fit(out)
    print "IDF:", tfidf.idf_
    tf_idf_matrix = tfidf.transform(out)
    return tf_idf_matrix.todense()

def tfidf_manually(corpus):
    c = idf(corpus)
    vec = CountVectorizer()
    out = vec.fit_transform(corpus).toarray()
    vocab = vec.vocabulary_
    docs = numpy.zeros(shape=(len(corpus),len(vocab)))

    for i in range(0,len(corpus)):
        li = corpus[i].split()
        for k,v in vocab.items():
            if k in li:
                docs[i][v]=round(c[k],4)
    return docs

def makeFeatureVec(opt,words, model, num_features,vec):
    featureVec = numpy.zeros((len(model['president']),),dtype="float32")
    nwords = 1.
    if opt == 0:
        index2word_set = set(model.wv.index2word)
#    print type(vec)  
        for word in words.split(): 
            if word.lower() in index2word_set:
                nwords = nwords + 1.
            #print vec.item(int(nwords)-1).shape
            #print model[word.lower()].shape
                featureVec = numpy.add(featureVec,model[word.lower()]*vec.item(int(nwords)-1))
    
    if opt == 1:
        for word in words.split():
            try:
                featureVec = numpy.add(featureVec,model[word.lower()])
            except:
                continue
    
    #featureVec = numpy.divide(featureVec,numpy.sum(vec))
    return featureVec


def getAvgFeatureVecs(opt,docs, model, num_features,tfidf):
    counter = 0.
    FeatureVecs = numpy.zeros((len(docs),len(model['president'])),dtype="float32")
    for doc in docs:
        FeatureVecs[int(counter)] = makeFeatureVec(opt,doc, model, num_features,tfidf[int(counter)])
        counter = counter + 1.
    return FeatureVecs


def glove(corpus,gloveFile,opt):
    print "Loading glove"
    docs = []
    for i in range(0,len(corpus)):
        doc = corpus[i].split()
        docs.append(doc)
    f = open(gloveFile,'r')
    model={}
    for line in f:
        splitLine = line.split()
        word = splitLine[0]
        embedding = numpy.array([float(val) for val in splitLine[1:]])
        model[word] = embedding
    print "Done.",len(model)," words loaded!"
    #model.save('glove')
    print len(model['hello'])

    if int(opt) == 0:
        vec = CountVectorizer()
        out = vec.fit_transform(corpus).toarray()
        vocab = vec.vocabulary_
        tfidf = numpy.ones(shape=(len(corpus),len(vocab)))
        print "0"
    num_features = 300
    #print tfidf
    trainDataVecs = getAvgFeatureVecs( 1,corpus, model, num_features,tfidf )
    return trainDataVecs
    

def word2vec(corpus,opt,saved=1):
    docs = []
    for i in range(0,len(corpus)):
        doc = corpus[i].split()
        docs.append(doc)
    if saved == 0:
       # logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s',\
        #level=logging.INFO)
        num_features = 300    # Word vector dimensionality
        min_word_count = 40   # Minimum word count
        num_workers = 4       # Number of threads to run in parallel
        context = 10          # Context window size
        downsampling = 1e-3   # Downsample setting for frequent words
        model = Word2Vec(docs, workers=num_workers, size=num_features, min_count = min_word_count, window = context, sample = downsampling, seed=1)
        model.save('word2vec')
    else:
        num_features = 300
        model = Word2Vec.load('word2vec')
   
    if opt == 0:
        vec = CountVectorizer()
        out = vec.fit_transform(corpus).toarray()
        vocab = vec.vocabulary_
        tfidf = numpy.ones(shape=(len(corpus),len(vocab)))
        print "0"
    elif opt == 1 :
        tfidf = tfidf_auto(corpus)
    trainDataVecs = getAvgFeatureVecs(0, corpus, model, num_features,tfidf )
    return trainDataVecs

def doc2vec(corpus): 
    import gensim
    model = gensim.models.doc2vec.Doc2Vec.load('doc2vec2')
    print model.infer_vector(corpus[2])
    docs = numpy.zeros(shape=(len(corpus),100))
    for i in range(0,len(corpus)):
        docs[i]=model.infer_vector(corpus[i])
    return docs

if __name__ == "__main__":
    with open('filteredtext.txt','r') as f:
        d = f.read()
        corpus = d.split('\n\n')
        corpus = corpus[:-1]
        #print corpus[0].split()
#        word2vec(corpus)
    print glove(corpus,'glove.6B.100d.txt',0)
#    model = Word2Vec.load('word2vec')
#    numpy.set_printoptions(threshold=numpy.nan)    
#    word2vec(corpus,0,1)
#    print listofwords(corpus,model,300)
#    b = term_frequency(corpus)
#    print(b)
#    c = idf(corpus)
#    print(c)
#    d = tfidf_auto(corpus)
#    print(d[1])
#    print(d[1][1])
#    print doc2vec(corpus)
 #   m=e_matrix(corpus,1)
 #   print m[1]
