import os
import glob
import io
from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize
from nltk.stem.porter import PorterStemmer
from nltk import pos_tag
from nltk.stem.wordnet import WordNetLemmatizer
from nltk.corpus import wordnet

path = 'Users/megha/Downloads'#/aclImdb/train/test'
#print path
base = os.getcwd()
path = os.path.join(base, 'test','pos')
path2 = os.path.join(base, 'test','neg')
stop_words = set(stopwords.words('english'))
stop_words.add('http')
#porter = PorterStemmer()
lmtzr = WordNetLemmatizer()


def tag(pos):
    if pos.startswith('N'):
        return wordnet.NOUN
    elif pos.startswith('V'):
        return wordnet.VERB
    elif pos.startswith('J'):
        return wordnet.ADJ
    elif pos.startswith('R'):
        return wordnet.ADV
count = 0 
for filename in glob.glob(os.path.join(path,'*.txt')):
    count=count+1
    afile = open('test-pos.txt','a')    
    with open(filename) as f:
        text=f.read()
        text = text.decode("utf8",'ignore')
        tokens = word_tokenize(text)
        words = [word for word in tokens if word.isalpha()]
        words = [w.lower() for w in words if not w.lower() in stop_words]
        #words = [porter.stem(word) for word in words]
        t = pos_tag(words) 
#        print(t)
        for (word,pos) in t:
            tagged = tag(pos)
            if tagged is None:
                lemma = lmtzr.lemmatize(word)
            else:
                lemma = lmtzr.lemmatize(word,tagged) 
            afile.write(" "+lemma.encode('utf-8'))
    afile.write('\n\n')
        

for filename in glob.glob(os.path.join(path2,'*.txt')):
    count=count+1
    afile = open('test-neg.txt','a')    
    with open(filename) as f:
        text=f.read()
        text = text.decode("utf8",'ignore')
        tokens = word_tokenize(text)
        words = [word for word in tokens if word.isalpha()]
        words = [w for w in words if not w.lower() in stop_words]
        #words = [porter.stem(word) for word in words]
        t = pos_tag(words) 
#        print(t)
        for (word,pos) in t:
            tagged = tag(pos)
            if tagged is None:
                lemma = lmtzr.lemmatize(word)
            else:
                lemma = lmtzr.lemmatize(word,tagged) 
            afile.write(" "+lemma.encode('utf-8'))
    afile.write('\n\n')
        
















#st = LancasterStemmer()
#for filename in os.listdir(path):
#    print filename
#stop_words = set(stopwords.words('english'))
#for filename in glob.glob(os.path.join(path, '*.txt')):
#    appendFile = open('filteredtext.txt','a')
#    with open(filename) as f:
#        doc = f.read()
#        words = doc.split()
#        for r in words: 
#            r = r.lower()
#            r = st.stem(r)
#            if not r in stop_words:
#                appendFile.write(" "+r)
                
#    appendFile.write('\n\n')
#appendFile.close()
    #print data[1]
