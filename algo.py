from sklearn.naive_bayes import MultinomialNB
from sklearn.pipeline import Pipeline
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.cross_validation import train_test_split
from bow import *
import numpy as np
from sklearn.naive_bayes import GaussianNB
from sklearn.linear_model import LogisticRegression
from sklearn import svm
from sklearn.neural_network import MLPClassifier
from sklearn import linear_model
import tensorflow as tf
import random
from keras.callbacks import Callback

def train(classifier, X, y):
    print len(X)
    idx = np.random.permutation(len(X))
    X,y = X[idx], y[idx]
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.25, random_state=33)
    classifier.fit(X_train, y_train)
    print "Accuracy: %s" % classifier.score(X_test, y_test)
    return classifier

class TestCallback(Callback):
    def __init__(self, test_data):
        self.test_data = test_data

    def on_epoch_end(self, epoch, logs={}):
        x, y = self.test_data
        loss, acc = self.model.evaluate(x, y, verbose=0)
        print('\nTesting loss: {}, acc: {}\n'.format(loss, acc))



def lstm(corpus,mat,y):
    from keras.utils import np_utils
    from keras.models import Sequential
    from keras.layers.core import Dense, Dropout, Activation, Lambda
    from keras.layers.embeddings import Embedding
    from keras.layers.recurrent import LSTM, SimpleRNN, GRU
    from keras.preprocessing.text import Tokenizer
    from collections import defaultdict
    from keras.preprocessing import sequence
    from keras.regularizers import l2

    maxlen = 100
    batch_size = 32
    nb_classes = 2
    idx = np.random.permutation(len(mat))
    mat,y = mat[idx], y[idx]
    np.set_printoptions(threshold=np.nan)
    y_train = y[1:20000]
    y_test = y[-4000:]
    X_train =mat[1:20000] #sequence.pad_sequences(sequences_train, maxlen=maxlen)
    X_test =mat[-4000:] #sequence.pad_sequences(sequences_test, maxlen=maxlen)
    from sklearn import preprocessing
    le = preprocessing.LabelEncoder()
    le.fit(y_train)
    y_train = le.transform(y_train)
    Y_train = np_utils.to_categorical(y_train, nb_classes)
    y_test = le.transform(y_test)
    Y_test = np_utils.to_categorical(y_test, nb_classes)

    X_train = X_train.reshape(-1,1,300)
    X_test = X_test.reshape(-1,1,300)
    
    model = Sequential()
    model.add(LSTM(8,input_shape=(1,300),dropout_U=0.2,dropout_W=0.2))
    model.add(Dense(2,activation='sigmoid'))
    model.add(Activation('softmax'))
    
    model.compile(loss='binary_crossentropy',
                  optimizer='adam',
                  metrics=['accuracy'])
    

    print('Train...')
    model.fit(X_train, Y_train, batch_size=batch_size, nb_epoch=15,
              validation_data=(X_test, Y_test),callbacks=[TestCallback((X_test, Y_test))])
    score, acc = model.evaluate(X_test, Y_test,
                                batch_size=batch_size)
    print('Test accuracy:', acc)
    
    
    #print("Generating test predictions...")
    preds = model.predict_classes(X_test, verbose=1)

def logistic(corpus,model,Y):
    clf = LogisticRegression()
    train(clf,model,Y)

def naive_bayes(corpus,model,Y):
    clf = GaussianNB()
    train(clf,model,Y)

def svm(corpus,model,Y):
    clf = linear_model.SGDClassifier(loss = 'hinge')
    train(clf,model,Y)

def nn(corpus,model,Y):
    clf = MLPClassifier(solver='sgd', alpha=1e-5,hidden_layer_sizes=(10,10), random_state=1)
    train(clf,model,Y)




def testbow(corpus):
    print "Initialising bag of word model"
    mat = bagofword(corpus)
    return mat

def testtf(corpus):
    print "Initialising term frequency model"
    mat = term_frequency(corpus)
    return mat

def testtfidf(corpus):
    print "INitialising tfidf model"
    mat = tfidf_auto(corpus)
    return mat

def testglove(corpus):
    print "INitialising glove model"
    mat=glove(corpus,'glove.6B.300d.txt',0)
    return mat

def testword2vec(corpus,opt):
    print "INitialising word2vec model"
    mat=word2vec(corpus,opt,1)
    return mat

def testdoc2vec(corpus):
    print "INitialising doc2vec model"
    mat=doc2vec(corpus)
    return mat

if __name__ == "__main__":
    with open('postext.txt','r') as f:
        d = f.read()
        corpus = d.split('\n\n')
        corpus = corpus[:-1]       
        Y = np.zeros(len(corpus))
    with open('negtext.txt','r') as f:
        d = f.read()
        corpus2 = d.split('\n\n')
        corpus2 = corpus2[:-1]       
        Y2 = numpy.ones(len(corpus2))
    Y=np.concatenate((Y,Y2),axis=0)
    corpus = corpus + corpus2 
   
    mat = doc2vec(corpus)
    model =mat
    print "---------------------Naive Bayes-----------------"
    naive_bayes(corpus,mat,Y)    
    print "-------------Logistic Regression-----------------"
    logistic(corpus,model,Y)
    print "----------------------SVM-------------------------"
    svm(corpus,model,Y)
    print "--------------------Neural Network----------------"
    nn(corpus,model,Y)
    print "-------------LSTM-----------"
    lstm(corpus,model,Y)
#    print "-------------GRU-----------"
